const WebSocket = require('ws');
const wss = new WebSocket.Server({ noServer: true });

wss.on('connection', function connection(ws, request) {
    ws.on('message', function message(msg) {
        console.log(`Received message ${msg} from user`);
    });
});

const wssConnect = function (request, socket, head) {
    wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request);
    });
}

module.exports = wssConnect;