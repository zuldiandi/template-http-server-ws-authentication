'use strict';

const http = require('http');

const safeParse = require('./function/safelyParseJson');
const wss = require('./websocket/websocket');
const db = require('./db-operation/db-operation');

const server = http.createServer();

server.on("request", (request, response) => {
  request.on('error', (err) => {
    console.error(err);
    response.statusCode = 404;
    response.end();
  });

  response.on('error', (err) => {
    console.error(err);
  });

  if (request.method === 'POST' && request.headers["content-type"] === "application/json") {
    respondRequestJSON(request, response);
  } else {
    response.statusCode = 404;
    response.end();
  }
}).listen(8888, "192.168.1.6");

server.on('upgrade', function upgrade(request, socket, head) {
  if (request.headers['upgrade'] === 'websocket') {
    var queryparams = request.url.split('?')[1];

    var params = queryparams.split('&');
    var pair = null, data = {};

    params.forEach(function (d) {
      pair = d.split('=');
      data[pair[0]] = pair[1];
    });

    if (data.username && data.password) {
      db.checkAuth([data.username, data.password],function (err, result) {
        if (err) console.log(err);
        else {
          if (result) {
            wss(request, socket, head);
          } else {
            socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
            socket.destroy();
            return;
          }
        }
      });
    }
  }
});

function respondRequestJSON(request, response) {
  var body = "";
  var jsonData = null;
  request.on('data', (chunk) => {
    body += chunk;
    if (body.length > 1e6) {
      body = "";
      response.writeHead(413, { 'Content-Type': 'text/plain' }).end();
      request.connection.destroy();
    }
  }).on('end', () => {
    jsonData = safeParse.safelyParseJSON(body);

    //here where every function should be
    if (jsonData) {
      switch (request.url) {
        case "/echo":
          response.end(body.toString());
          break;
        case "/saveToken":
          insertTokenDB(jsonData, response);
          break;
        default:
          response.statusCode = 404;
          response.end();
      }
    } else {
      response.statusCode = 400;
      response.end();
    }
  });
}

function insertTokenDB(jsonData, response) {
  db.checkAuth([jsonData.username, jsonData.password], function (err, result) {
    if (err) {
      console.log(err);
      response.statusCode = 400;
      response.end();
    } else if (result) {
      console.log(result[0]);
      var objToken = {
        token: jsonData.token,
        mac_address: jsonData.mac_address,
        id_user: result[0].id
      }

      db.insertToken(objToken, function (err, result) {
        if (err) console.log(err);
        else {
          console.log("CHECK DB");
          response.end();
        }
      });
    } else {
      response.statusCode = 401;
      response.end();
    }
  });
}