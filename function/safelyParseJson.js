function safelyParseJSON(json) {
    var parsed;

    try {
        parsed = JSON.parse(json)
    } catch (e) {
        if (e instanceof SyntaxError) {
            console.log("Data Selain JSON diterima, datanya : ",json);
        }
        return false;
    }
    return parsed; // Could be undefined!
}

exports.safelyParseJSON = safelyParseJSON;