const connectDB = require('./db-config');

var checkAuth = function (arrPayload, callback) {
    var db = connectDB();

    db.query({
        sql: "SELECT * from testuser WHERE `username` = ? and `password` = ?",
        timeout: 40000,
        values: arrPayload
    }, function (error, result, field) {
        if (error) return callback("Database Error");
        if (result.length) {
            return callback(null, result);
        } else {
            return callback(null, false);
        }
    });

    db.end();
}

var insertToken = function(objPayload, callback){
    var db = connectDB();

    db.query({
        sql:"INSERT INTO token SET ?",
        timeout: 40000,
        values: objPayload
    }, function (error, result, field){
        if(error) return callback(error);
        return callback(null, true);
    });

    db.end();
}

module.exports = {
    checkAuth,
    insertToken
};